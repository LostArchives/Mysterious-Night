﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class LoadingScreenManager : MonoBehaviour {

    public static LoadingScreenManager Instance;
    public float fadeDuration;
    public static string LoadingScene = "LoadingScreen";
    private Canvas MyCanvas;
    private Image Fader;
    private AsyncOperation async;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);

        Fader = transform.GetChild(0).GetComponent<Image>();
        MyCanvas = GetComponent<Canvas>();
        Fader.CrossFadeAlpha(0, 0f, true);

    }

	// Use this for initialization
	void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SceneLoad(string sceneName)
    {
        StartCoroutine(LoadScene(sceneName));
    }

    private IEnumerator LoadScene(string sceneName)
    {

        Fader.CrossFadeAlpha(1, fadeDuration, true);

        yield return new WaitForSeconds(fadeDuration);

        async = SceneManager.LoadSceneAsync(LoadingScene);
        MyCanvas.renderMode = RenderMode.ScreenSpaceCamera;
        MyCanvas.worldCamera = GameObject.Find("Main Camera").GetComponent<Camera>();

        yield return new WaitUntil( () => async.isDone);

        Fader.CrossFadeAlpha(0, fadeDuration, true);

        yield return new WaitForSeconds(fadeDuration);

        async = SceneManager.LoadSceneAsync(sceneName,LoadSceneMode.Additive);
        async.allowSceneActivation = false;
        

        yield return new WaitUntil(() => async.progress>=.9f);

        Fader.CrossFadeAlpha(1, fadeDuration, true);

        yield return new WaitForSeconds(fadeDuration);
        async.allowSceneActivation = true;

        yield return new WaitUntil(() => async.isDone);

        Fader.CrossFadeAlpha(0, fadeDuration, true);

        yield return new WaitForSeconds(fadeDuration);

        MyCanvas.renderMode = RenderMode.ScreenSpaceCamera;
        MyCanvas.worldCamera = GameObject.Find("Main Camera").GetComponent<Camera>();

    }
}
