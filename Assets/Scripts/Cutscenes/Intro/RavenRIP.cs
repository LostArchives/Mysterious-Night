﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RavenRIP : MonoBehaviour {

    [SerializeField] Image Fader;
    [SerializeField] GameObject Raven;
    [SerializeField] GameObject ZombieHand;
    [SerializeField] GameObject Grave;
    [SerializeField] AudioClip[] AudioClips;
   
    private Animator ravenAnimator;
    private Animator zombieHandAnimator;
    private Animator graveAnimator;
    private AudioSource zombieHandAudio;
    private AudioSource graveAudio;

    void Awake()
    {
        ravenAnimator = Raven.GetComponent<Animator>();
        zombieHandAnimator = ZombieHand.GetComponent<Animator>();
        graveAnimator = Grave.GetComponent<Animator>();
        zombieHandAudio = ZombieHand.GetComponent<AudioSource>();
        graveAudio = Grave.GetComponent<AudioSource>();
    }

	// Use this for initialization
	void Start () {
        Fader.CrossFadeAlpha(1f, 0f, true);
        StartCoroutine(PlayIntro());

	}
	
	// Update is called once per frame
	void Update () {
	
	}


    private IEnumerator PlayIntro()
    {
        Fader.CrossFadeAlpha(0f, 3f, true);

        SleepRaven();

        yield return new WaitForSeconds(2f);

        yield return StartCoroutine(ZombieAnnoyRavenFirst());

        yield return new WaitForSeconds(2f);

        yield return StartCoroutine(ZombieAnnoyRavenSecond());

        yield return new WaitForSeconds(2f);

        yield return StartCoroutine(RavenCounter());

        yield return new WaitForSeconds(1f);

        LoadingScreenManager.Instance.SceneLoad("TitleScreen");
    }

    private IEnumerator ZombieAnnoyRavenFirst()
    {
        PlayZombieAudio(true);

        DigZombie(1, true);

        yield return new WaitForSeconds(2f);
        AwakeRaven();

        yield return new WaitForSeconds(2f);

        LookRaven(true);

        yield return new WaitForSeconds(.2f);

        DigZombie(1, false);

        PlayZombieAudio(false);

        for (int cntLook = 0; cntLook < 2; cntLook++)
        {
            LookRaven(false);
            yield return new WaitForSeconds(1f);
            LookRaven(true);
            yield return new WaitForSeconds(1f);
        }

        yield return new WaitForSeconds(.5f);

        SleepRaven();

        yield return null;
    }

    private IEnumerator ZombieAnnoyRavenSecond()
    {
        PlayZombieAudio(true);

        DigZombie(2, true);

        yield return new WaitForSeconds(1f);
        AwakeRaven();

        yield return new WaitForSeconds(1f);

        LookRaven(true);

        yield return new WaitForSeconds(1f);

        LookRaven(false);

        AnnoyedRaven(true);

        yield return new WaitForSeconds(.5f);

        DigZombie(2, false);

        PlayZombieAudio(false);

       

        yield return null;
    }

    private IEnumerator RavenCounter()
    {
        
        JumpRaven(true);
        graveAudio.clip = AudioClips[2];
        graveAudio.loop = true;
        graveAudio.Play();

        yield return new WaitForSeconds(4f);

        PlayZombieAudio(true);

        DigZombie(3, true);

        JumpRaven(false);

        yield return new WaitForSeconds(1f);
        graveAudio.Stop();
        graveAudio.loop = false;

        FallGrave();
        Raven.GetComponent<RavenFly>().enabled = true;

        PlayZombieAudio(false);

        DigZombie(3, false);

        yield return new WaitForSeconds(1f);

        CalmRaven();

        yield return new WaitForSeconds(2f);
        Raven.GetComponent<RavenFly>().ResetWings();
        Raven.GetComponent<RavenFly>().enabled = false;


        yield return null;
    }



    #region ShortCut Methods

    private void AwakeRaven()
    {
        ravenAnimator.SetTrigger("Awake");
    }

    private void SleepRaven()
    {
        ravenAnimator.SetTrigger("Sleep");
    }

    private void AnnoyedRaven(bool annoyed)
    {
        ravenAnimator.SetBool("Annoyed",annoyed);
    }

    private void LookRaven(bool up)
    {
        if (up)
            ravenAnimator.SetTrigger("LookUp");
        else
            ravenAnimator.SetTrigger("LookDown");
    }

    private void JumpRaven(bool jump)
    {
        ravenAnimator.SetBool("Jump", jump);
    }

    private void CalmRaven()
    {
        ravenAnimator.SetTrigger("Calm");
        AnnoyedRaven(false);
    }

    private void DigZombie(int part, bool start)
    {
        zombieHandAnimator.SetBool("Dig" + part , start);
    }

    private void FallGrave()
    {
        graveAnimator.SetTrigger("Fall");
        graveAudio.clip = AudioClips[3];
        graveAudio.Play();
    }

    private void PlayZombieAudio(bool start)
    {
        if (start)
        {
            zombieHandAudio.clip = AudioClips[1];
            zombieHandAudio.Play();
        }
        else
        {
            zombieHandAudio.Stop();
        }
        
    }

    #endregion

}
