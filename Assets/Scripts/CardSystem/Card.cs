﻿using UnityEngine;

public class Card {

    private Sprite _front;
    private Sprite _back;
    private int _value;

    public Sprite Front
    {
        get { return _front; }
        set { _front = value; }
    }

    public Sprite Back
    {
        get { return _back; }
        set { _back = value; }
    }

    public int Value
    {
        get { return _value; }
        set { _value = value; }
    }

}
