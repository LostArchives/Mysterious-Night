﻿using UnityEngine;
using System.Collections;

public class TitleScreen : MonoBehaviour {

    [SerializeField] GameObject _witch;
    [SerializeField] Animator[] _smoke;
    [SerializeField] Animator[] _eyes;
    [SerializeField] GameObject _title;

    // Use this for initialization
    void Start () {
        StartCoroutine(LaunchTitleScreen());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private IEnumerator LaunchTitleScreen()
    {
        
        FlyWitch(true);
        ShowSmoke(true);

        yield return new WaitForSeconds(1f);

        Vector3 lastPosition;
        Vector3 newPosition;
        do
        {
            lastPosition = _witch.transform.position;
            yield return new WaitForSeconds(.01f);
            newPosition = _witch.transform.position;

        } while (lastPosition != newPosition);

        ShowSmoke(false);

        yield return new WaitForSeconds(.5f);

        ShowTitle(true);

        yield return new WaitForSeconds(.5f);

        BlinkEyes(true);

        yield return null;
    }

    #region ShortCut Methods

    private void ShowSmoke(bool show)
    {
        for (int cntSmoke = 0; cntSmoke < _smoke.Length; cntSmoke ++)
        {
            Animator anim = _smoke[cntSmoke].GetComponent<Animator>();
            anim.SetBool("Show", show);
        }
    }

    private void FlyWitch(bool fly)
    {
        Animator anim = _witch.GetComponent<Animator>();
        anim.SetBool("Show", fly);
    }

    private void ShowTitle(bool show)
    {
        Animator anim = _title.GetComponent<Animator>();
        anim.SetBool("Show", show);
    }

    private void BlinkEyes(bool blink)
    {
        for (int cntEye = 0; cntEye < _eyes.Length; cntEye++)
        {
            Animator anim = _eyes[cntEye].GetComponent<Animator>();
            anim.SetBool("Blink", blink);
        }
    }

    #endregion
}
